package com.masterdmitres.jstart.logic;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class MailQueue {

    private final Queue<String> queue = new LinkedBlockingQueue<>();

    public void add(String value) {
        queue.add(value);
    }

    public String poll() {
        return queue.poll();
    }

}
