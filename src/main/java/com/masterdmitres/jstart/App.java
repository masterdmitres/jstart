package com.masterdmitres.jstart;

public class App {

    public static void main(String[] args) {

        Application app = new Application();
        app.init();
        app.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            app.stop();
        }));


    }

}
