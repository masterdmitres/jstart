package com.masterdmitres.jstart.threads;

import com.masterdmitres.jstart.logic.MailQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WriterThread extends AbstractCycle {

    private static final Logger log = LoggerFactory.getLogger("writer");

    private final MailQueue queue;

    public WriterThread(MailQueue queue) {
        super(log);
        this.queue = queue;
    }

    @Override
    protected void init() {

    }

    @Override
    protected long execute() {

        String value = queue.poll();
        if (value == null) {
            return 1000;
        }
        log.debug("got {}", value);
        return 0;
    }

    @Override
    protected void terminate() {

    }


}
