package com.masterdmitres.jstart.threads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractCycle implements Runnable {


    private final Logger log;
    private final String threadName;

    private volatile boolean running = false;
    private final Object stopflag = new Object();

    public AbstractCycle(Logger log) {
        this.log = log;
        this.threadName = trim(log.getName());
    }

    protected abstract void init();

    protected abstract long execute();

    protected abstract void terminate();

    public final void start() {

        log.info("starting thread: {} ...", threadName);

        running = true;

        Thread thread = new Thread(this);
        thread.setDaemon(false);
        thread.start();

        log.info("thread started: {} OK", threadName);

    }

    public final void stop() {

        long startTimestamp = System.currentTimeMillis();
        log.info("waiting for stopping: {} ...", threadName);

        running = false;
        try {
            synchronized (stopflag) {
                stopflag.wait(30000);
            }
        } catch (Exception e) {
            log.error("fail to stop", e);
        }

        long finishTimestamp = System.currentTimeMillis();
        log.info("cycling stopped: {}, t: {}", threadName, finishTimestamp - startTimestamp);

    }

    @Override
    public final void run() {

        try {
            log.info("initialization: {} ...", threadName);
            init();
            log.info("initialized: {} OK", threadName);
        } catch (Exception e) {
            running = false; // не запускаемся если инициализация не прошла
            log.error("fail to start", e);
        }

        log.info("execution cycling {} ...", threadName);

        while (running) {
            try {
                if (allowCycle()) {
                    long delay = execute();
                    sleep(delay);
                } else {
                    sleep(100);
                }
            } catch (Exception e) {
                log.error("failed to execute", e);
            }
        }

        try {
            log.info("terminating subclass {} ...", threadName);
            terminate();
            log.info("subclass terminated OK {}", threadName);
        } catch (Exception e) {
            log.error("fail to terminate", e);
        }

        synchronized (stopflag) {
            stopflag.notify();
        }

        log.info("cycling terminated: {}", threadName);

    }

    private void sleep(long delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException ignore) {
        }
    }

    protected boolean allowCycle() {
        return true;
    }

    private static String trim(final String name) {
        return name.length() > 20 ? name.substring(0, 20) : name;
    }
}
