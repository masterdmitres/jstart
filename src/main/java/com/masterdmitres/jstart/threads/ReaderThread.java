package com.masterdmitres.jstart.threads;

import com.masterdmitres.jstart.logic.MailQueue;
import com.masterdmitres.jstart.threads.AbstractCycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.UUID;

public class ReaderThread extends AbstractCycle {

    private static final Logger log = LoggerFactory.getLogger("reader");

    private final MailQueue queue;

    public ReaderThread(MailQueue queue) {
        super(log);
        this.queue = queue;
    }

    @Override
    protected void init() {
    }

    @Override
    protected long execute() {
        String value = UUID.randomUUID().toString();
        log.debug("create {}", value);
        queue.add(value);
        return 500;
    }

    @Override
    protected void terminate() {
    }

}
