package com.masterdmitres.jstart;

import com.masterdmitres.jstart.logic.MailQueue;
import com.masterdmitres.jstart.threads.AbstractCycle;
import com.masterdmitres.jstart.threads.ReaderThread;
import com.masterdmitres.jstart.threads.WriterThread;

import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;

public class Application {

    private final Collection<AbstractCycle> cycles = new LinkedBlockingQueue<>();

    public void init() {

        MailQueue mailQueue = new MailQueue();

        ReaderThread demo = new ReaderThread(mailQueue);
        WriterThread writer = new WriterThread(mailQueue);

        cycles.add(demo);
        cycles.add(writer);

    }

    public void start() {
        for (AbstractCycle cycle : cycles) {
            cycle.start();
        }
    }

    public void stop() {
        for (AbstractCycle cycle : cycles) {
            cycle.stop();
        }
    }
}
